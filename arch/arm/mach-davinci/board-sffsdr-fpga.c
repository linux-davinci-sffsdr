/*
 * SFFSDR-board specific FPGA driver
 *
 * Copyright (C) 2008 Lyrtech <www.lyrtech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/string.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/fpgadl.h>

#include <asm/gpio.h>

#include <mach/sffsdr-fpga.h>

#define MODULE_NAME "sffsdr_fpga"

/* Used to determine if the bitstream is loaded. */
#define FPGA_DEVICE_NAME "fpgadl_par0"

/* Define this to have verbose debug messages. */
#define SFFSDR_FPGA_DEBUG 1

#ifdef SFFSDR_FPGA_DEBUG
#define DBGMSG(fmt, args...)					\
	printk(KERN_INFO "%s: "fmt"\n" , MODULE_NAME, ## args)
#define FAILMSG(fmt, args...)					\
	printk(KERN_ERR "%s: "fmt"\n" , MODULE_NAME, ## args)
#else
#define DBGMSG(fmt, args...)
#define FAILMSG(fmt, args...)
#endif

#define FPGA_FULL_RESET_VAL	3
#define FPGA_PARTIAL_RESET_VAL	2

#define FPGA_DS2_ON		(1<<0)
#define FPGA_DS3_ON		(1<<1)
#define FPGA_DS4_ON		(1<<2)
#define FPGA_DS5_ON		(1<<3)
#define FPGA_DS6_ON		(1<<4)

/* Sampling frequency divider, bits 5:4 */
#define FPGA_FS_DIV_BY_1	(0<<4)
#define FPGA_FS_DIV_BY_2	(1<<4)
#define FPGA_FS_DIV_BY_4	(2<<4)
#define FPGA_FS_DIV_RSV		(3<<4)

/* Sampling rate selection, bit 2 */
#define FPGA_SR_STANDARD	(0<<2) /* Standard sampling rate, default */
#define FPGA_SR_DOUBLE		(1<<2) /* Double sampling rate */

/* Sampling frequency selection, bits 1:0 */
#define FPGA_FS_48000		(0<<0) /* 48.0 kHz (PLL 12.288 MHz) */
#define FPGA_FS_44100		(1<<0) /* 44.1 kHz (PLL 11.2896 MHz) */
#define FPGA_FS_32000		(2<<0) /* 32.0 kHz (PLL 8.192 MHz) */
#define FPGA_FS_RSV		(3<<0) /* Reserved */

struct sffsdr_fpga_dev_t {
	int bitstream_mode;
	u16 *regs;
};

static enum {
	SFFSDR_FPGA_STATE_START,
	SFFSDR_FPGA_STATE_DRV_STRUCT_ALLOCATED,
	SFFSDR_FPGA_STATE_REGS_MAPPED,
} sffsdr_fpga_state;

struct sffsdr_fpga_dev_t *sffsdr_fpga_dev;

/* The EMIF address lines A0 to A2 are not routed to the
 * FPGA. Therefore, the upper 16 bits are never valid. */
u16 sffsdr_fpga_regread(int offset)
{
	return sffsdr_fpga_dev->regs[offset / 2];
}
EXPORT_SYMBOL(sffsdr_fpga_regread);

void sffsdr_fpga_regwrite(int offset, u16 value)
{
	sffsdr_fpga_dev->regs[offset / 2] = value;
}
EXPORT_SYMBOL(sffsdr_fpga_regwrite);

/* Reset the inside logic of the FPGA according to the
 * bitstream mode. This is done when the bitstream has
 * been programmed and is Lyrtech SFF-SDR specific. */
static void sffsdr_fpga_reset(int bitstream_mode)
{
	u32 value;

	if (bitstream_mode == BITSTREAM_MODE_FULL)
		value = FPGA_FULL_RESET_VAL;
	else
		value = FPGA_PARTIAL_RESET_VAL;

	sffsdr_fpga_regwrite(SFFSDR_FPGA_GLOBAL_CTRL, value);
	sffsdr_fpga_regwrite(SFFSDR_FPGA_GLOBAL_CTRL, 0);
}

static int sffsdr_fpga_post_load(int bitstream_mode)
{
	DBGMSG("sffsdr_fpga_post_load()");

	if (fpgadl_is_bitstream_loaded(FPGA_DEVICE_NAME) < 1) {
		FAILMSG("  FPGA is not programmed");
		return -ENODEV;
	}

	sffsdr_fpga_reset(bitstream_mode);

	DBGMSG("FPGA Revision: %d",
	       sffsdr_fpga_regread(SFFSDR_FPGA_REVISION));

	/* Light some LEDs to indicate success. */
	sffsdr_fpga_regwrite(SFFSDR_FPGA_LED_CONTROL, FPGA_DS2_ON |
			     FPGA_DS3_ON | FPGA_DS4_ON | FPGA_DS5_ON |
			     FPGA_DS6_ON);

	/* Set default CODEC clock values. */
	sffsdr_fpga_regwrite(SFFSDR_FPGA_PLL_CODEC, FPGA_FS_DIV_BY_1 |
			     FPGA_FS_44100 | FPGA_SR_STANDARD);

	return 0;
}

int sffsdr_fpga_set_codec_fs(int fs)
{
	u16 fs_mask;

	if (fpgadl_is_bitstream_loaded(FPGA_DEVICE_NAME) < 1) {
		FAILMSG("FPGA is not programmed");
		return -ENODEV;
	}

	switch (fs) {
	case 32000:
		fs_mask = FPGA_FS_32000;
		break;
	case 44100:
		fs_mask = FPGA_FS_44100;
		break;
	case 48000:
		fs_mask = FPGA_FS_48000;
		break;
	default:
		FAILMSG("Unsupported sampling frequency");
		return -EFAULT;
		break;
	}

	sffsdr_fpga_regwrite(SFFSDR_FPGA_PLL_CODEC, FPGA_FS_DIV_BY_1 |
			     fs_mask | FPGA_SR_STANDARD);

	return 0;
}
EXPORT_SYMBOL(sffsdr_fpga_set_codec_fs);

static void sffsdr_fpga_cleanup(void)
{
	switch (sffsdr_fpga_state) {
	case SFFSDR_FPGA_STATE_REGS_MAPPED:
		iounmap(sffsdr_fpga_dev->regs);
	case SFFSDR_FPGA_STATE_DRV_STRUCT_ALLOCATED:
		kfree(sffsdr_fpga_dev);
	case SFFSDR_FPGA_STATE_START:
		/* Nothing to do. */
		break;
	}
}

static int __devinit sffsdr_fpga_probe(struct platform_device *pdev)
{
	struct resource *fpgaregs_res;
	int len;
	int result;

	DBGMSG("sffsdr_fpga_probe()");

	sffsdr_fpga_state = SFFSDR_FPGA_STATE_START;

	sffsdr_fpga_dev = kzalloc(sizeof(*sffsdr_fpga_dev), GFP_KERNEL);
	if (!sffsdr_fpga_dev) {
		FAILMSG("Failed to allocate device structure");
		result = -ENOMEM;
		goto error;
	}
	sffsdr_fpga_state = SFFSDR_FPGA_STATE_DRV_STRUCT_ALLOCATED;

	pdev->dev.driver_data = sffsdr_fpga_dev; /* Private driver data */

	/* Assign virtual addresses to FPGAREGS I/O memory regions. */
	fpgaregs_res = platform_get_resource_byname(pdev, IORESOURCE_MEM,
						    "sffsdr_regs");
	if (!fpgaregs_res) {
		FAILMSG("Error getting fpgaregs ressource");
		result = -ENODEV;
		goto error;
	}
	len = fpgaregs_res->end - fpgaregs_res->start;
	sffsdr_fpga_dev->regs = ioremap(fpgaregs_res->start, len);
	if (!sffsdr_fpga_dev->regs) {
		FAILMSG("Can't remap fpgaregs registers");
		result = -ENXIO;
		goto error;
	}
	sffsdr_fpga_state = SFFSDR_FPGA_STATE_REGS_MAPPED;

	/* Temporary... */
	sffsdr_fpga_post_load(BITSTREAM_MODE_FULL);

	return 0;

error:
	sffsdr_fpga_cleanup();
	return result;
}

static int __devexit sffsdr_fpga_remove(struct platform_device *pdev)
{
	DBGMSG("sffsdr_fpga_remove()");
	sffsdr_fpga_cleanup();

	return 0;
}

static struct platform_driver sffsdr_fpga_platform_driver = {
	.driver         = {
		.name   = MODULE_NAME,
		.owner  = THIS_MODULE,
	},
	.remove = sffsdr_fpga_remove,
};

static int __init sffsdr_fpga_init(void)
{
	int res;

	DBGMSG("sffsdr_fpga_init()");

	res = platform_driver_probe(&sffsdr_fpga_platform_driver,
				    sffsdr_fpga_probe);
	if (res) {
		DBGMSG("platform_driver_probe() failed");
		return res;
	}

	return 0;
}
module_init(sffsdr_fpga_init);

static void __exit sffsdr_fpga_exit(void)
{
	DBGMSG("sffsdr_fpga_exit()");
	platform_driver_unregister(&sffsdr_fpga_platform_driver);
}
module_exit(sffsdr_fpga_exit);

MODULE_AUTHOR("Hugo Villeneuve <hvilleneuve@lyrtech.com>");
MODULE_DESCRIPTION("Lyrtech SFFSDR SX-35 FPGA driver");
MODULE_LICENSE("GPL");
