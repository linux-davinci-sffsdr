/*
 * Lyrtech SFFSDR board support.
 *
 * Copyright (C) 2008 Philip Balister, OpenSDR <philip@opensdr.com>
 * Copyright (C) 2008 Lyrtech <www.lyrtech.com>
 *
 * Based on DV-EVM platform, original copyright follows:
 *   Copyright (C) 2007 MontaVista Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/i2c/at24.h>
#include <linux/etherdevice.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/io.h>
#include <linux/phy.h>
#include <linux/fpgadl.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>

#include <mach/dm644x.h>
#include <mach/common.h>
#include <mach/emac.h>
#include <mach/i2c.h>
#include <mach/serial.h>
#include <mach/mmc.h>
#include <mach/psc.h>
#include <mach/mux.h>
#include <mach/nand.h>
#include <mach/mmc.h>
#include <mach/sffsdr-lyrvpfe.h>

#define XC4VSX35_PAYLOAD_SIZE	(1707240)

#define DAVINCI_ASYNC_EMIF_CONTROL_BASE		0x01e00000
#define DAVINCI_ASYNC_EMIF_DATA_CE0_BASE	0x02000000

#define DAVINCI_VPSS_REGS_BASE			0x01C70000

#define LXT971_PHY_ID				0x001378e2
#define LXT971_PHY_MASK				0xfffffff0

#define FPGA_SELECTMAP_BASE			0x04000000
#define FPGA_SFFSDR_REGS_BASE			0x04008000

/* DDR2 memory is 256 Mbytes */
#define DDR2_BASE		0x80000000

#define SFFSDR_MMC_CD_PIN	GPIO(51)
#define SFFSDR_MMC_RO_PIN	GPIO(50)

struct mtd_partition davinci_sffsdr_nandflash_partition[] = {
	/* U-Boot Environment: Block 0
	 * UBL:                Block 1
	 * U-Boot:             Blocks 6-7 (256 kb)
	 * Integrity Kernel:   Blocks 8-31 (3 Mb)
	 * Integrity Data:     Blocks 100-END
	 */
	{
		.name		= "Linux Kernel",
		.offset		= 32 * SZ_128K,
		.size		= 16 * SZ_128K, /* 2 Mb */
		.mask_flags	= MTD_WRITEABLE, /* Force read-only */
	},
	{
		.name		= "Linux ROOT",
		.offset		= MTDPART_OFS_APPEND,
		.size		= 256 * SZ_128K, /* 32 Mb */
		.mask_flags	= 0, /* R/W */
	},
};

static struct davinci_nand_pdata davinci_sffsdr_nandflash_data = {
	.parts		= davinci_sffsdr_nandflash_partition,
	.nr_parts	= ARRAY_SIZE(davinci_sffsdr_nandflash_partition),
	.ecc_mode	= NAND_ECC_HW,
};

static struct resource davinci_sffsdr_nandflash_resource[] = {
	{
		.start		= DAVINCI_ASYNC_EMIF_DATA_CE0_BASE,
		.end		= DAVINCI_ASYNC_EMIF_DATA_CE0_BASE + SZ_16M - 1,
		.flags		= IORESOURCE_MEM,
	}, {
		.start		= DAVINCI_ASYNC_EMIF_CONTROL_BASE,
		.end		= DAVINCI_ASYNC_EMIF_CONTROL_BASE + SZ_4K - 1,
		.flags		= IORESOURCE_MEM,
	},
};

static struct platform_device davinci_sffsdr_nandflash_device = {
	.name		= "davinci_nand", /* Name of driver */
	.id		= 0,
	.dev		= {
		.platform_data	= &davinci_sffsdr_nandflash_data,
	},
	.num_resources	= ARRAY_SIZE(davinci_sffsdr_nandflash_resource),
	.resource	= davinci_sffsdr_nandflash_resource,
};

static struct at24_platform_data eeprom_info = {
	.byte_len	= (64*1024) / 8,
	.page_size	= 32,
	.flags		= AT24_FLAG_ADDR16,
};

static struct i2c_board_info __initdata i2c_info[] =  {
	{
		I2C_BOARD_INFO("24lc64", 0x50),
		.platform_data	= &eeprom_info,
	},
	/* Other I2C devices:
	 * MSP430,  addr 0x23 (not used)
	 * PCA9543, addr 0x70 (setup done by U-Boot)
	 * ADS7828, addr 0x48 (ADC for voltage monitoring.)
	 */
};

static struct davinci_i2c_platform_data i2c_pdata = {
	.bus_freq	= 20 /* kHz */,
	.bus_delay	= 100 /* usec */,
};

static void __init sffsdr_init_i2c(void)
{
	davinci_init_i2c(&i2c_pdata);
	i2c_register_board_info(1, i2c_info, ARRAY_SIZE(i2c_info));
}

static int sffsdr_mmc_get_cd(int module)
{
	return gpio_get_value(SFFSDR_MMC_CD_PIN);
}

static int sffsdr_mmc_get_ro(int module)
{
	return gpio_get_value(SFFSDR_MMC_RO_PIN);
}

static struct davinci_mmc_config sffsdr_mmc_config = {
	.get_cd		= sffsdr_mmc_get_cd,
	.get_ro		= sffsdr_mmc_get_ro,
	.wires		= 4
};

/*
 * The FPGA is loaded using the SelectMAP mode through
 * the EMIF interface and some dedicated control signals:
 *
 *   FPGA          DM6446
 *   --------------------
 *   PROGRAM_B     GPIO37
 *   DONE          GPIO39
 *   INIT          GPIO40
 *   DOUT_BUSY     GPIO42 (Not used)
 *   CS_B          EMIF_A13 OR CS3n
 */
static struct fpgadl_pdata_t fpgadl_par_pdata = {
	.fpga_family		= FPGA_FAMILY_XILINX_XC4V,
	.payload_full_size	= XC4VSX35_PAYLOAD_SIZE,
	.program_b		= GPIO(37),
	.done			= GPIO(39),
	.init_b			= GPIO(40),
	.bitstream_name		= "fpga.bit",
	.check_init_low		= 0,
};

/* FPGA physical EMIF register resources. */
static struct resource davinci_fpgadl_par_resources[] = {
	{
		.name           = "selectmap",
		.start		= FPGA_SELECTMAP_BASE,
		.end		= FPGA_SELECTMAP_BASE + 4 - 1,
		.flags		= IORESOURCE_MEM,
	},
};

static struct platform_device davinci_fpgadl_par_device = {
	.name		= "fpgadl_par", /* Name of driver */
	.id		= 0,
	.dev		= {
		.platform_data	= &fpgadl_par_pdata,
	},
	.num_resources	= ARRAY_SIZE(davinci_fpgadl_par_resources),
	.resource	= davinci_fpgadl_par_resources,
};

/* SFFSDR specific FPGA registers. */
static struct resource davinci_sffsdr_fpga_resources[] = {
	{
		.name           = "sffsdr_regs",
		.start		= FPGA_SFFSDR_REGS_BASE,
		.end		= FPGA_SFFSDR_REGS_BASE + SZ_1K - 1,
		.flags		= IORESOURCE_MEM,
	},
};

static struct platform_device davinci_sffsdr_fpga_device = {
	.name		= "sffsdr_fpga", /* Name of driver */
	.id		= -1, /* Only one instance = -1 */
	.num_resources	= ARRAY_SIZE(davinci_sffsdr_fpga_resources),
	.resource	= davinci_sffsdr_fpga_resources,
};

static struct lyrvpfe_platform_data lyrvpfe_pdata = {
	/*
	 * GPIO(1) for DSP  to FPGA (VPBE)
	 * GPIO(0) for FPGA to DSP  (VPFE)
	 */
	.ready_gpio = GPIO(0), /* DSP to FPGA (VPFE) */
};

static struct resource lyrvpfe_resources[] = {
	{
		.name           = "regs",
		.start          = DAVINCI_VPSS_REGS_BASE,
		.end            = DAVINCI_VPSS_REGS_BASE + SZ_16K - 1,
		.flags          = IORESOURCE_MEM,
	},
	{
		.name           = "irq",
		.start          = IRQ_VDINT0,
		.end            = IRQ_VDINT0,
		.flags          = IORESOURCE_IRQ,
	},
};

static struct platform_device lyrvpfe_pdev = {
	.name           = "lyrvpfe",
	.id             = 0,
	.dev		= {
		.platform_data	= &lyrvpfe_pdata,
	},
	.resource       = lyrvpfe_resources,
	.num_resources  = ARRAY_SIZE(lyrvpfe_resources),
};

static struct platform_device *davinci_sffsdr_devices[] __initdata = {
	&davinci_fpgadl_par_device, /* Bitstream loading - parallel */
	&davinci_sffsdr_fpga_device, /* Application functionality */
	&lyrvpfe_pdev,
};

/*
 * UART0: console
 * UART1: FPGA
 */
static struct davinci_uart_config uart_config __initdata = {
	.enabled_uarts = DAVINCI_UART0_ENA | DAVINCI_UART1_ENA,
};

static void __init davinci_sffsdr_map_io(void)
{
	davinci_map_common_io();
	dm644x_init();
}

static int davinci_phy_fixup(struct phy_device *phydev)
{
	unsigned int control;
	/* CRITICAL: Fix for increasing PHY signal drive strength for
	 * TX lockup issue. On DaVinci EVM, the Intel LXT971 PHY
	 * signal strength was low causing  TX to fail randomly. The
	 * fix is to Set bit 11 (Increased MII drive strength) of PHY
	 * register 26 (Digital Config register) on this phy. */
	control = phy_read(phydev, 26);
	phy_write(phydev, 26, (control | 0x800));
	return 0;
}

static void __init davinci_sffsdr_init(void)
{
	gpio_request(SFFSDR_MMC_CD_PIN, "MMC CD");
	gpio_direction_input(SFFSDR_MMC_CD_PIN);
	gpio_request(SFFSDR_MMC_RO_PIN, "MMC RO");
	gpio_direction_input(SFFSDR_MMC_RO_PIN);

	/* Turn UART1 MUX ON. */
	davinci_cfg_reg(DM644X_UART1);

	platform_add_devices(davinci_sffsdr_devices,
			     ARRAY_SIZE(davinci_sffsdr_devices));
	sffsdr_init_i2c();

	davinci_serial_init(&uart_config);

#if defined(CONFIG_MTD_NAND_DAVINCI) || \
    defined(CONFIG_MTD_NAND_DAVINCI_MODULE)
	davinci_cfg_reg(DM644X_HPIEN_DISABLE);
	davinci_cfg_reg(DM644X_ATAEN_DISABLE);
	platform_device_register(&davinci_sffsdr_nandflash_device);
#endif

	davinci_setup_mmc(0, &sffsdr_mmc_config);

	/* Register the fixup for PHY on DaVinci */
	phy_register_fixup_for_uid(LXT971_PHY_ID, LXT971_PHY_MASK,
					davinci_phy_fixup);

	davinci_init_emac(NULL);

	setup_usb(0, 0); /* We support only peripheral mode. */
}

static void __init davinci_sffsdr_irq_init(void)
{
	davinci_irq_init();
}

MACHINE_START(SFFSDR, "Lyrtech SFFSDR")
	/* Maintainer: Hugo Villeneuve hugo.villeneuve@lyrtech.com */
	.phys_io      = IO_PHYS,
	.io_pg_offst  = (__IO_ADDRESS(IO_PHYS) >> 18) & 0xfffc,
	.boot_params  = (DAVINCI_DDR_BASE + 0x100),
	.map_io	      = davinci_sffsdr_map_io,
	.init_irq     = davinci_sffsdr_irq_init,
	.init_machine = davinci_sffsdr_init,
	.timer	      = &davinci_timer,
MACHINE_END
