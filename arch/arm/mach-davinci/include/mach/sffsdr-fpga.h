/*
 * sffsdr_fpga.h
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SFFSDR_FPGA_H
#define __SFFSDR_FPGA_H

#define SFFSDR_FPGA_REVISION			0x000
#define SFFSDR_FPGA_GLOBAL_CTRL			0x040
#define SFFSDR_FPGA_LED_CONTROL			0x300
#define SFFSDR_FPGA_PLL_CODEC			0x800

/* VPSS configuration register */
#define SFFSDR_FPGA_VPSS_CONTROL		0xA00

/* VPSS, VPBE packet size configuration register */
#define SFFSDR_FPGA_VPSS_FROM_DSP_FIFO		0xA40

/* VPSS, VPFE packet size configuration register */
#define SFFSDR_FPGA_VPSS_TO_DSP_FIFO		0xA80

/* VPSS, VPFE number of lines configuration register */
#define SFFSDR_FPGA_VPSS_LINES_PER_FRAME	0xAC0

#define SFFSDR_FPGA_CUSTOM_REG0_LSB		0xC00
#define SFFSDR_FPGA_CUSTOM_REG0_MSB		0xC20

u16 sffsdr_fpga_regread(int offset);

void sffsdr_fpga_regwrite(int offset, u16 value);

int sffsdr_fpga_set_codec_fs(int fs);

#endif /* __SFFSDR_FPGA_H */
